/*

	wl-toplevel-qt
	Copyright (C) 2020  Abrar

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

#include <QDebug>

#include <wayland-client.h>
#include <wayland-util.h>

#include "registry.h"

static wl_seat *seat = nullptr;
static wl_display *display = nullptr;

static void registry_add_object(void *data, wl_registry *registry, uint32_t name, const char *interface, uint32_t version);
static void registry_remove_object(void *data, struct wl_registry *registry, uint32_t name);

Registry::Registry(QObject *parent)
	: QObject(parent)
{
	display = wl_display_connect(nullptr);

	if (!display) {
		qDebug() << "No Wayland Display found.";
		return;
	}

	wl_registry *registry = wl_display_get_registry(display);
	static struct wl_registry_listener registry_listener = {
		&registry_add_object,
		&registry_remove_object
	};
	wl_registry_add_listener(registry, &registry_listener, this);
	wl_display_roundtrip(display);

	RegistryThread *rt = new RegistryThread;
	// Update current state
	while (wl_display_dispatch(display) != -1 && manager()->isFinished()) {
		qDebug() << "manage" << manager()->isFinished();
	}
	// Running forever
	rt->start();
}

ToplevelManager *Registry::manager()
{
	return m_manager;
}

void Registry::setManager(ToplevelManager *manager)
{
	m_manager = manager;
}

ToplevelManager::ToplevelManager(struct ::zwlr_foreign_toplevel_manager_v1 *object, QObject *parent)
	: QObject(parent)
	, QtWayland::zwlr_foreign_toplevel_manager_v1(object)
{
	qDebug() << "Toplevel Manager Constructor";
	m_handleList.clear();
	m_isFinished = false;
}

bool ToplevelManager::isFinished()
{
	return m_isFinished;
}

ToplevelHandleList ToplevelManager::handleList()
{
	return m_handleList;
}

void ToplevelManager::zwlr_foreign_toplevel_manager_v1_toplevel(struct ::zwlr_foreign_toplevel_handle_v1 *handle)
{
	ToplevelHandle *h = new ToplevelHandle(handle);
	m_handleList.append(h);
	connect(h, &ToplevelHandle::handleClosed, this, &ToplevelManager::removeHandle);
	emit handleListUpdated(); // TODO : Update single handle not full list
}

void ToplevelManager::zwlr_foreign_toplevel_manager_v1_finished()
{
	qDebug() << "Manager finished";
	m_isFinished = true;
	zwlr_foreign_toplevel_manager_v1_destroy(this->object());
}

void ToplevelManager::removeHandle(ToplevelHandle *handle)
{
	m_handleList.removeOne(handle);
	emit handleListUpdated();
}

ToplevelHandle::ToplevelHandle(struct ::zwlr_foreign_toplevel_handle_v1 *object, QObject *parent)
	: QObject(parent)
	, QtWayland::zwlr_foreign_toplevel_handle_v1(object)
{
	qDebug() << "Toplevel Handle Constructor";
	m_isDone = false;
	m_isClosed = false;
}

QString ToplevelHandle::appTitle()
{
	return m_title;
}

QString ToplevelHandle::appID()
{
	return m_appid;
}

uint32_t ToplevelHandle::appState()
{
	return m_state;
}

bool ToplevelHandle::isDone()
{
	return m_isDone;
}

bool ToplevelHandle::isClosed()
{
	return m_isClosed;
}

void ToplevelHandle::minimizeHandle()
{
	qDebug() << "Trying to minimize " << m_appid;
	set_minimized();
	wl_display_roundtrip(display);
}

void ToplevelHandle::unsetMinimizeHandle()
{
	unset_minimized();
	wl_display_roundtrip(display);
}

void ToplevelHandle::maximizeHandle()
{
	qDebug() << "Trying to maximize " << m_appid;
	set_maximized();
	wl_display_roundtrip(display);
}

void ToplevelHandle::unsetMaximizeHandle()
{
	unset_maximized();
	wl_display_roundtrip(display);
}

void ToplevelHandle::activateHandle()
{
	qDebug() << "Trying to activate " << m_appid;
	activate(seat);
	wl_display_roundtrip(display);

//	while (wl_display_dispatch(display) != -1 && !m_isDone) {
//		qDebug() << "Waiting for activate";
	//  }
}

void ToplevelHandle::closeHandle()
{
	close();
	wl_display_roundtrip(display);
}

void ToplevelHandle::zwlr_foreign_toplevel_handle_v1_title(const QString &title)
{
	m_title = title;
}

void ToplevelHandle::zwlr_foreign_toplevel_handle_v1_app_id(const QString &app_id)
{
	m_appid = app_id;
}

void ToplevelHandle::zwlr_foreign_toplevel_handle_v1_output_enter(wl_output *output)
{
	Q_UNUSED(output)
}

void ToplevelHandle::zwlr_foreign_toplevel_handle_v1_output_leave(wl_output *output)
{
	Q_UNUSED(output)
}

void ToplevelHandle::zwlr_foreign_toplevel_handle_v1_state(wl_array *state)
{
	m_state = 0;

	auto *states = static_cast<uint32_t *>(state->data);
	int numStates = static_cast<int>(state->size / sizeof(uint32_t));

	for (int i = 0; i < numStates; i++) {
		if (states[i] == ZWLR_FOREIGN_TOPLEVEL_HANDLE_V1_STATE_ACTIVATED) {
			m_state |= Activated;
		} else if (states[i] == ZWLR_FOREIGN_TOPLEVEL_HANDLE_V1_STATE_MAXIMIZED) {
			m_state |= Maximized;
		} else if (states[i] == ZWLR_FOREIGN_TOPLEVEL_HANDLE_V1_STATE_MINIMIZED) {
			m_state |= Minimized;
		}
	}
}

void ToplevelHandle::zwlr_foreign_toplevel_handle_v1_done()
{
	qDebug() << m_appid << "Done";
	m_isDone = true;
	emit handleUpdated(this);
}

void ToplevelHandle::zwlr_foreign_toplevel_handle_v1_closed()
{
	qDebug() << m_appid << "Closed";
	m_isClosed = true;
	emit handleClosed(this);
	destroy();
}
static int count = 0;
static void registry_add_object(void *data, wl_registry *registry, uint32_t name, const char *interface, uint32_t version)
{
	if (strcmp(interface, zwlr_foreign_toplevel_manager_v1_interface.name) == 0) {
		auto m = static_cast<zwlr_foreign_toplevel_manager_v1 *>(
					 wl_registry_bind(registry, name, &zwlr_foreign_toplevel_manager_v1_interface, 1));

		Registry *mm = static_cast<Registry *>(data);
		qDebug() << "Manager " << ++count;
		mm->setManager(new ToplevelManager(m));
	} else if (strcmp(interface, wl_seat_interface.name) == 0 && !seat) {
		seat = static_cast<wl_seat *>(wl_registry_bind(registry, name, &wl_seat_interface, version));
	}
}

static void registry_remove_object(void *data, struct wl_registry *registry, uint32_t name)
{
	Q_UNUSED(data)
	qDebug() << "Removing registry object " << name;
	wl_registry_destroy(registry);
}

RegistryThread::RegistryThread()
	: QThread()
{
}

void RegistryThread::run()
{
	while (wl_display_dispatch(display) != -1) {
	}
}
