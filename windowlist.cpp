/*

	wl-toplevel-qt
	Copyright (C) 2020  Abrar

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

#include <QTimer>
#include <QListWidgetItem>
#include <QDebug>

#include "registry.h"

#include "windowlist.h"
#include "ui_windowlist.h"

WindowList::WindowList(QWidget *parent) :
	QWidget(parent),
	ui(new Ui::WindowList)
{
	ui->setupUi(this);
	ui->activate->setEnabled(false);

	qRegisterMetaType<ToplevelHandle*>("ToplevelHandle");

	QTimer::singleShot(500, this, &WindowList::init);
}

WindowList::~WindowList()
{
	delete ui;
}

void WindowList::on_close_clicked()
{
	QListWidgetItem *item = ui->listWidget->currentItem();
	if (!item) {
		qDebug() << "No item";
		return;
	}

	ToplevelHandle *handle = item->data(Qt::UserRole).value<ToplevelHandle*>();
	if (!handle) {
		qDebug() << "Empty handle";
		return;
	}

	qDebug() << "Trying to close...";
	handle->closeHandle();
}

void WindowList::on_activate_clicked()
{
	QListWidgetItem *item = ui->listWidget->currentItem();
	if (!item) {
		qDebug() << "No item";
		return;
	}

	ToplevelHandle *handle = item->data(Qt::UserRole).value<ToplevelHandle*>();
	if (!handle) {
		qDebug() << "Empty handle";
		return;
	}

	handle->activateHandle();
}

void WindowList::on_listWidget_currentRowChanged(int currentRow)
{
	QListWidgetItem *item = ui->listWidget->item(currentRow);
	if (!item) {
		qDebug() << "No item";
		return;
	}

	ToplevelHandle *handle = item->data(Qt::UserRole).value<ToplevelHandle*>();
	if (!handle) {
		qDebug() << "Empty handle";
		return;
	}

	ui->close->setEnabled(true);
	updateStateText(handle);
}

void WindowList::init()
{
	m_registry = new Registry;
	qDebug() << "Registry...";

	connect(m_registry->manager(), &ToplevelManager::handleListUpdated, this, &WindowList::updateWindowList);
	updateWindowList();
}

void WindowList::updateWindowList()
{
	ui->listWidget->clear();

	ToplevelHandleList handleList = m_registry->manager()->handleList();
	QListWidgetItem *item;
	for (const auto &h : handleList) {
		item = new QListWidgetItem(QString("%1 (%2)").arg(h->appID()).arg(h->appTitle()));
		item->setData(Qt::UserRole, QVariant::fromValue(h));
		connect(h, &ToplevelHandle::handleUpdated, this, &WindowList::updateStateText);
		ui->listWidget->addItem(item);
	}
}

void WindowList::updateStateText(ToplevelHandle *handle)
{
	QString s = "";
	if (handle->appState() & ToplevelHandle::Activated) {
		s += "Activated, ";
		ui->activate->setEnabled(false);
	} else {
		s += "Not Activated, ";
		ui->activate->setEnabled(true);
	}

	if (handle->appState() & ToplevelHandle::Maximized) {
		s += "Maximized, ";
		ui->maximize->setChecked(true);
	} else {
		s += "Not Maximized, ";
		ui->maximize->setChecked(false);
	}

	if (handle->appState() & ToplevelHandle::Minimized) {
		s += "Minimized";
		ui->minimize->setChecked(true);
	} else {
		s += "Not Minimized";
		ui->minimize->setChecked(false);
	}

	ui->stat->setText("State: " + s);
}

void WindowList::on_minimize_clicked(bool checked)
{
	QListWidgetItem *item = ui->listWidget->currentItem();
	if (!item) {
		qDebug() << "No item";
		return;
	}

	ToplevelHandle *handle = item->data(Qt::UserRole).value<ToplevelHandle*>();
	if (!handle) {
		qDebug() << "Empty handle";
		return;
	}

	if (checked) {
		handle->unsetMinimizeHandle();
	} else {
		handle->minimizeHandle();
	}
}

void WindowList::on_maximize_clicked(bool checked)
{
	QListWidgetItem *item = ui->listWidget->currentItem();
	if (!item) {
		qDebug() << "No item";
		return;
	}

	ToplevelHandle *handle = item->data(Qt::UserRole).value<ToplevelHandle*>();
	if (!handle) {
		qDebug() << "Empty handle";
		return;
	}

	if (checked) {
		handle->unsetMaximizeHandle();
	} else {
		handle->maximizeHandle();
	}
}
