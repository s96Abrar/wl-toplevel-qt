QT += widgets

CONFIG += c++11

CONFIG += link_pkgconfig wayland-scanner
WAYLANDCLIENTSOURCES += \
    wlr-foreign-toplevel-management-unstable-v1.xml

QMAKE_USE += wayland-client


SOURCES += \
        main.cpp \
        registry.cpp \
        windowlist.cpp

unix {
    target.path = /usr/bin
    INSTALLS += target
}

HEADERS += \
    registry.h \
    windowlist.h

FORMS += \
    windowlist.ui
