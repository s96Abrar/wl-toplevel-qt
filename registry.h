/*

	wl-toplevel-qt
	Copyright (C) 2020  Abrar

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

#pragma once

#include <QObject>
#include <QThread>
#include "qwayland-wlr-foreign-toplevel-management-unstable-v1.h"

class Registry;
class ToplevelManager;
class ToplevelHandle;

typedef QList<ToplevelHandle*> ToplevelHandleList;

class ToplevelHandle : public QObject, public QtWayland::zwlr_foreign_toplevel_handle_v1
{
	Q_OBJECT
public:
	enum ToplevelState {
		Activated = (1 << 0),
		Maximized = (1 << 1),
		Minimized = (1 << 2)
	};

	ToplevelHandle(struct ::zwlr_foreign_toplevel_handle_v1 *object, QObject *parent = nullptr);

	QString appTitle();
	QString appID();
	uint32_t appState();
	bool isDone();
	bool isClosed();

	void minimizeHandle();
	void unsetMinimizeHandle();
	void maximizeHandle();
	void unsetMaximizeHandle();
	void activateHandle();
	void closeHandle();

signals:
	void handleUpdated(ToplevelHandle *handle);
	void handleClosed(ToplevelHandle *handle);

protected:
	void zwlr_foreign_toplevel_handle_v1_title(const QString &title) override;
	void zwlr_foreign_toplevel_handle_v1_app_id(const QString &app_id) override;
	void zwlr_foreign_toplevel_handle_v1_output_enter(struct ::wl_output *output) override;
	void zwlr_foreign_toplevel_handle_v1_output_leave(struct ::wl_output *output) override;
	void zwlr_foreign_toplevel_handle_v1_state(wl_array *state) override;
	void zwlr_foreign_toplevel_handle_v1_done() override;
	void zwlr_foreign_toplevel_handle_v1_closed() override;

private:
	QString m_title;
	QString m_appid;
	uint32_t m_state;
	bool m_isDone;
	bool m_isClosed;
};
Q_DECLARE_METATYPE(ToplevelHandle*)

class ToplevelManager : public QObject, public QtWayland::zwlr_foreign_toplevel_manager_v1 {
	Q_OBJECT
public:
	ToplevelManager(struct ::zwlr_foreign_toplevel_manager_v1 *object, QObject *parent = nullptr);

	bool isFinished();
	ToplevelHandleList handleList();

signals:
	void handleListUpdated();

protected:
	void zwlr_foreign_toplevel_manager_v1_toplevel(struct ::zwlr_foreign_toplevel_handle_v1 *handle);
	void zwlr_foreign_toplevel_manager_v1_finished() override;

private:
	ToplevelHandleList m_handleList;
	bool m_isFinished;

	void removeHandle(ToplevelHandle *handle);
};

class RegistryThread : public QThread
{
	Q_OBJECT

public:
	RegistryThread();

protected:
	void run();
};

class Registry : public QObject
{
	Q_OBJECT
public:
	Registry(QObject *parent = nullptr);

	ToplevelManager *manager();
	void setManager(ToplevelManager *manager);

private:
	ToplevelManager *m_manager;
};
