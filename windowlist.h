/*

	wl-toplevel-qt
	Copyright (C) 2020  Abrar

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

#pragma once

#include <QWidget>

#include "registry.h"

class QListWidgetItem;

namespace Ui {
class WindowList;
}

class WindowList : public QWidget
{
	Q_OBJECT

public:
	explicit WindowList(QWidget *parent = nullptr);
	~WindowList();

private slots:
	void on_close_clicked();
	void on_activate_clicked();
	void on_listWidget_currentRowChanged(int currentRow);
	void on_minimize_clicked(bool checked);
	void on_maximize_clicked(bool checked);

private:
	Ui::WindowList *ui;
	Registry *m_registry;

	void init();
	void updateWindowList();
	void updateStateText(ToplevelHandle *handle);
};

